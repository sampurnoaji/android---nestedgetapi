package com.example.pegadaiantest;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    private Service service;
    private Button btnGet;

    private RecyclerView recyclerView;
    private List<Integer> listId;
    private AdapterList adapterList;
    private ModelStory modelStory;
    private List<ModelStory> listModel = new ArrayList<>();

    private static Retrofit retrofit;
    private static String BASE_URL = "https://hacker-news.firebaseio.com/";

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        service = getRetrofitInstance().create(Service.class);
        bindToLayout();

        progressDialog = new ProgressDialog(MainActivity.this);
        btnGet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog.show();
                requestId();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        for (int i = 0; i < 10; i++) {
                            requestStory(i);
                        }
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                progressDialog.dismiss();
                                Toast.makeText(MainActivity.this, ""+listModel.get(0).getBy(), Toast.LENGTH_SHORT).show();
                                recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false));
                                adapterList = new AdapterList(getApplicationContext(), listModel);
                                recyclerView.setAdapter(adapterList);
                            }
                        }, 1500);
                    }
                }, 1500);
            }
        });
    }

    private void bindToLayout(){
        btnGet = findViewById(R.id.btn_get);
        recyclerView = findViewById(R.id.rv);
    }

    private void requestId(){
        service.getId("https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty")
                .enqueue(new Callback<List<Integer>>() {
                    @Override
                    public void onResponse(Call<List<Integer>> call, Response<List<Integer>> response) {
                        listId = response.body();
                    }

                    @Override
                    public void onFailure(Call<List<Integer>> call, Throwable t) {
                        progressDialog.dismiss();
                        Toast.makeText(MainActivity.this, "Internet Connection Problem", Toast.LENGTH_SHORT).show();
                    }
                });
    }
    private void requestStory(int position) {
        service.getStory("https://hacker-news.firebaseio.com/v0/item/"+ listId.get(position) +".json?print=pretty")
//        service.getStory("https://hacker-news.firebaseio.com/v0/item/20818779.json?print=pretty")
                .enqueue(new Callback<ModelStory>() {
                    @Override
                    public void onResponse(Call<ModelStory> call, Response<ModelStory> response) {
                        modelStory = response.body();
                        listModel.add(modelStory);
                    }

                    @Override
                    public void onFailure(Call<ModelStory> call, Throwable t) {
                        progressDialog.dismiss();
                        Toast.makeText(MainActivity.this, "Internet Connection Problem", Toast.LENGTH_SHORT).show();
                    }
                });
//        return modelStory;
    }

    public static Retrofit getRetrofitInstance(){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build();

        if (retrofit == null){
            retrofit = new retrofit2.Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build();
        }
        return retrofit;
    }

}
