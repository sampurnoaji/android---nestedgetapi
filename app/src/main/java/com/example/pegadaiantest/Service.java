package com.example.pegadaiantest;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Dibuat oleh petersam
 */
public interface Service {
    @GET
    Call<ModelStory> getStory(@Url String url);

    @GET
    Call<List<Integer>> getId(@Url String url);

}
