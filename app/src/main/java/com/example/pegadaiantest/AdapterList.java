package com.example.pegadaiantest;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

/**
 * Dibuat oleh petersam
 */
public class AdapterList extends RecyclerView.Adapter<HolderList> {
    private Context context;
    private List<ModelStory> list;

    public AdapterList(Context context, List<ModelStory> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public HolderList onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_list, parent, false);
        return new HolderList(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HolderList holder, int position) {
        ModelStory modelStory = list.get(position);
        holder.id.setText(modelStory.getId().toString());
        holder.by.setText(modelStory.getBy());
        holder.title.setText(modelStory.getTitle());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
