package com.example.pegadaiantest;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Dibuat oleh petersam
 */
public class HolderList extends RecyclerView.ViewHolder {
    TextView id, by, title;

    public HolderList(@NonNull View itemView) {
        super(itemView);
        id = itemView.findViewById(R.id.card_id);
        by = itemView.findViewById(R.id.card_by);
        title = itemView.findViewById(R.id.card_title);
    }
}
